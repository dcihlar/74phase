EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5F1B8965
P 3600 2500
AR Path="/5F1B8965" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B8965" Ref="#PWR05"  Part="1" 
AR Path="/61B07F66/5F1B8965" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B8965" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 3600 2250 50  0001 C CNN
F 1 "GND" H 3605 2327 50  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:VDC #PWR?
U 1 1 5F1B896B
P 3600 1100
AR Path="/5F1B896B" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B896B" Ref="#PWR04"  Part="1" 
AR Path="/61B07F66/5F1B896B" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B896B" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 3600 1000 50  0001 C CNN
F 1 "VDC" H 3615 1273 50  0000 C CNN
F 2 "" H 3600 1100 50  0001 C CNN
F 3 "" H 3600 1100 50  0001 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2100 3300 2100
Wire Wire Line
	3300 2000 2350 2000
$Comp
L power:GND #PWR?
U 1 1 5F1B8977
P 3600 4700
AR Path="/5F1B8977" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B8977" Ref="#PWR07"  Part="1" 
AR Path="/61B07F66/5F1B8977" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B8977" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 3600 4450 50  0001 C CNN
F 1 "GND" H 3605 4527 50  0000 C CNN
F 2 "" H 3600 4700 50  0001 C CNN
F 3 "" H 3600 4700 50  0001 C CNN
	1    3600 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 4300 3300 4300
Wire Wire Line
	3300 4200 2350 4200
$Comp
L power:GND #PWR?
U 1 1 5F2EBFDD
P 3100 1700
AR Path="/5F2EBFDD" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F2EBFDD" Ref="#PWR0103"  Part="1" 
AR Path="/61B07F66/5F2EBFDD" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F2EBFDD" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 3100 1450 50  0001 C CNN
F 1 "GND" H 3105 1527 50  0000 C CNN
F 2 "" H 3100 1700 50  0001 C CNN
F 3 "" H 3100 1700 50  0001 C CNN
	1    3100 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1700 4100 1700
Wire Wire Line
	4100 1700 4100 1850
Wire Wire Line
	3900 2200 4100 2200
Wire Wire Line
	4100 2200 4100 2150
Wire Wire Line
	4000 1200 4100 1200
Wire Wire Line
	4100 1200 4100 1700
Connection ~ 4100 1700
Wire Wire Line
	3700 1200 3600 1200
Wire Wire Line
	3600 1200 3600 1100
Wire Wire Line
	3600 1200 3600 1500
Connection ~ 3600 1200
Wire Wire Line
	3600 1200 3100 1200
Wire Wire Line
	3100 1200 3100 1300
Wire Wire Line
	3900 3900 4100 3900
Wire Wire Line
	4100 3900 4100 4050
Wire Wire Line
	3900 4400 4100 4400
Wire Wire Line
	4100 4400 4100 4350
$Comp
L power:GND #PWR?
U 1 1 5F3B74F5
P 3100 3900
AR Path="/5F3B74F5" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F3B74F5" Ref="#PWR0104"  Part="1" 
AR Path="/61B07F66/5F3B74F5" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F3B74F5" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 3100 3650 50  0001 C CNN
F 1 "GND" H 3105 3727 50  0000 C CNN
F 2 "" H 3100 3900 50  0001 C CNN
F 3 "" H 3100 3900 50  0001 C CNN
	1    3100 3900
	1    0    0    -1  
$EndComp
Connection ~ 4100 3900
Connection ~ 4100 4400
Wire Wire Line
	3900 4500 4650 4500
Wire Wire Line
	3900 2300 4650 2300
Connection ~ 4100 2200
Wire Wire Line
	3900 1800 4650 1800
Wire Wire Line
	3900 4000 4650 4000
Wire Wire Line
	5100 4600 5000 4600
Wire Wire Line
	5000 4600 5000 4700
$Comp
L power:GND #PWR?
U 1 1 5F8BAADB
P 5000 4700
AR Path="/5F8BAADB" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F8BAADB" Ref="#PWR0114"  Part="1" 
AR Path="/61B07F66/5F8BAADB" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F8BAADB" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 5000 4450 50  0001 C CNN
F 1 "GND" H 5005 4527 50  0000 C CNN
F 2 "" H 5000 4700 50  0001 C CNN
F 3 "" H 5000 4700 50  0001 C CNN
	1    5000 4700
	1    0    0    -1  
$EndComp
Text HLabel 5100 4000 2    50   Output ~ 0
V.HO
Text HLabel 5100 4400 2    50   Output ~ 0
V.HS
Text HLabel 5100 4500 2    50   Output ~ 0
V.LO
Text HLabel 5100 4600 2    50   Output ~ 0
V.LS
Text HLabel 5100 1800 2    50   Output ~ 0
U.HO
Text HLabel 5100 2200 2    50   Output ~ 0
U.HS
Text HLabel 5100 2300 2    50   Output ~ 0
U.LO
Text HLabel 5100 2400 2    50   Output ~ 0
U.LS
$Comp
L power:GND #PWR?
U 1 1 5F8D3458
P 5000 2500
AR Path="/5F8D3458" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F8D3458" Ref="#PWR0115"  Part="1" 
AR Path="/61B07F66/5F8D3458" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F8D3458" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 5000 2250 50  0001 C CNN
F 1 "GND" H 5005 2327 50  0000 C CNN
F 2 "" H 5000 2500 50  0001 C CNN
F 3 "" H 5000 2500 50  0001 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2400 5000 2400
Wire Wire Line
	5000 2400 5000 2500
Wire Wire Line
	4950 1800 5100 1800
Wire Wire Line
	4950 4000 5100 4000
Wire Wire Line
	4950 2300 5100 2300
Wire Wire Line
	4100 2200 5100 2200
Wire Wire Line
	4950 4500 5100 4500
Wire Wire Line
	4100 4400 5100 4400
$Comp
L Device:R R14
U 1 1 5F172351
P 4800 2300
AR Path="/5F184EC4/5F172351" Ref="R14"  Part="1" 
AR Path="/61B07F66/5F172351" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F172351" Ref="R14"  Part="1" 
F 0 "R14" H 4870 2346 50  0000 L CNN
F 1 "4.7" H 4870 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 2300 50  0001 C CNN
F 3 "~" H 4800 2300 50  0001 C CNN
F 4 "C17675" H 4800 2300 50  0001 C CNN "LCSC"
	1    4800 2300
	0    -1   1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5F1725E3
P 4800 4500
AR Path="/5F184EC4/5F1725E3" Ref="R16"  Part="1" 
AR Path="/61B07F66/5F1725E3" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F1725E3" Ref="R16"  Part="1" 
F 0 "R16" H 4870 4546 50  0000 L CNN
F 1 "4.7" H 4870 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 4500 50  0001 C CNN
F 3 "~" H 4800 4500 50  0001 C CNN
F 4 "C17675" H 4800 4500 50  0001 C CNN "LCSC"
	1    4800 4500
	0    -1   1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5F1648DB
P 4800 4000
AR Path="/5F184EC4/5F1648DB" Ref="R15"  Part="1" 
AR Path="/61B07F66/5F1648DB" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F1648DB" Ref="R15"  Part="1" 
F 0 "R15" H 4870 4046 50  0000 L CNN
F 1 "4.7" H 4870 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 4000 50  0001 C CNN
F 3 "~" H 4800 4000 50  0001 C CNN
F 4 "C17675" H 4800 4000 50  0001 C CNN "LCSC"
	1    4800 4000
	0    -1   1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5F127460
P 4800 1800
AR Path="/5F184EC4/5F127460" Ref="R13"  Part="1" 
AR Path="/61B07F66/5F127460" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F127460" Ref="R13"  Part="1" 
F 0 "R13" H 4870 1846 50  0000 L CNN
F 1 "4.7" H 4870 1755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 1800 50  0001 C CNN
F 3 "~" H 4800 1800 50  0001 C CNN
F 4 "C17675" H 4800 1800 50  0001 C CNN "LCSC"
	1    4800 1800
	0    -1   1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5F3B74EB
P 3100 3650
AR Path="/5F184EC4/5F3B74EB" Ref="C7"  Part="1" 
AR Path="/61B07F66/5F3B74EB" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F3B74EB" Ref="C7"  Part="1" 
F 0 "C7" H 3215 3696 50  0000 L CNN
F 1 "10u" H 3215 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3138 3500 50  0001 C CNN
F 3 "~" H 3100 3650 50  0001 C CNN
F 4 "C13585" H 3100 3650 50  0001 C CNN "LCSC"
	1    3100 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5F380A03
P 4100 4200
AR Path="/5F184EC4/5F380A03" Ref="C10"  Part="1" 
AR Path="/61B07F66/5F380A03" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F380A03" Ref="C10"  Part="1" 
F 0 "C10" H 4215 4246 50  0000 L CNN
F 1 "10u" H 4215 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4138 4050 50  0001 C CNN
F 3 "~" H 4100 4200 50  0001 C CNN
F 4 "C13585" H 4100 4200 50  0001 C CNN "LCSC"
	1    4100 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5F2CCE21
P 4100 2000
AR Path="/5F184EC4/5F2CCE21" Ref="C9"  Part="1" 
AR Path="/61B07F66/5F2CCE21" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F2CCE21" Ref="C9"  Part="1" 
F 0 "C9" H 4215 2046 50  0000 L CNN
F 1 "10u" H 4215 1955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4138 1850 50  0001 C CNN
F 3 "~" H 4100 2000 50  0001 C CNN
F 4 "C13585" H 4100 2000 50  0001 C CNN "LCSC"
	1    4100 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5F2BF039
P 3100 1450
AR Path="/5F184EC4/5F2BF039" Ref="C6"  Part="1" 
AR Path="/61B07F66/5F2BF039" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F2BF039" Ref="C6"  Part="1" 
F 0 "C6" H 3215 1496 50  0000 L CNN
F 1 "10u" H 3215 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3138 1300 50  0001 C CNN
F 3 "~" H 3100 1450 50  0001 C CNN
F 4 "C13585" H 3100 1450 50  0001 C CNN "LCSC"
	1    3100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 5F2BE8B6
P 3850 1200
AR Path="/5F184EC4/5F2BE8B6" Ref="D1"  Part="1" 
AR Path="/61B07F66/5F2BE8B6" Ref="D?"  Part="1" 
AR Path="/61B81A35/5F2BE8B6" Ref="D1"  Part="1" 
F 0 "D1" H 3850 983 50  0000 C CNN
F 1 "US1M" H 3850 1074 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 3850 1200 50  0001 C CNN
F 3 "~" H 3850 1200 50  0001 C CNN
F 4 "C412437" H 3850 1200 50  0001 C CNN "LCSC"
	1    3850 1200
	-1   0    0    1   
$EndComp
$Comp
L Driver_FET:IR2184 U?
U 1 1 5F1B89EC
P 3600 4200
AR Path="/5F1B89EC" Ref="U?"  Part="1" 
AR Path="/5F184EC4/5F1B89EC" Ref="U6"  Part="1" 
AR Path="/61B07F66/5F1B89EC" Ref="U?"  Part="1" 
AR Path="/61B81A35/5F1B89EC" Ref="U6"  Part="1" 
F 0 "U6" H 3700 4650 50  0000 C CNN
F 1 "IR2184" H 3800 3750 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3600 4200 50  0001 C CIN
F 3 "" H 3600 4200 50  0001 C CNN
F 4 "C2995" H 3600 4200 50  0001 C CNN "LCSC"
	1    3600 4200
	1    0    0    -1  
$EndComp
$Comp
L Driver_FET:IR2184 U?
U 1 1 5F1B895F
P 3600 2000
AR Path="/5F1B895F" Ref="U?"  Part="1" 
AR Path="/5F184EC4/5F1B895F" Ref="U5"  Part="1" 
AR Path="/61B07F66/5F1B895F" Ref="U?"  Part="1" 
AR Path="/61B81A35/5F1B895F" Ref="U5"  Part="1" 
F 0 "U5" H 3700 2450 50  0000 C CNN
F 1 "IR2184" H 3800 1550 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3600 2000 50  0001 C CIN
F 3 "" H 3600 2000 50  0001 C CNN
F 4 "C2995" H 3600 2000 50  0001 C CNN "LCSC"
	1    3600 2000
	1    0    0    -1  
$EndComp
Text HLabel 2350 2000 0    50   Input ~ 0
U.IN
Text HLabel 2350 2100 0    50   Input ~ 0
U.~SD
Text HLabel 2350 4200 0    50   Input ~ 0
V.IN
Text HLabel 2350 4300 0    50   Input ~ 0
V.~SD
$Comp
L Device:D D2
U 1 1 5F3AD457
P 3850 3400
AR Path="/5F184EC4/5F3AD457" Ref="D2"  Part="1" 
AR Path="/61B07F66/5F3AD457" Ref="D?"  Part="1" 
AR Path="/61B81A35/5F3AD457" Ref="D2"  Part="1" 
F 0 "D2" H 3850 3183 50  0000 C CNN
F 1 "US1M" H 3850 3274 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 3850 3400 50  0001 C CNN
F 3 "~" H 3850 3400 50  0001 C CNN
F 4 "C412437" H 3850 3400 50  0001 C CNN "LCSC"
	1    3850 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 3400 3600 3400
Wire Wire Line
	3600 3400 3600 3700
Connection ~ 3600 3400
Wire Wire Line
	3600 3300 3600 3400
Wire Wire Line
	4100 3400 4100 3900
Wire Wire Line
	4000 3400 4100 3400
Wire Wire Line
	3100 3400 3100 3500
Wire Wire Line
	3600 3400 3100 3400
$Comp
L power:VDC #PWR?
U 1 1 5F1B897D
P 3600 3300
AR Path="/5F1B897D" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B897D" Ref="#PWR06"  Part="1" 
AR Path="/61B07F66/5F1B897D" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B897D" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 3600 3200 50  0001 C CNN
F 1 "VDC" H 3615 3473 50  0000 C CNN
F 2 "" H 3600 3300 50  0001 C CNN
F 3 "" H 3600 3300 50  0001 C CNN
	1    3600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3900 3100 3800
Wire Wire Line
	3100 1700 3100 1600
Wire Wire Line
	3100 6150 3100 6050
Text HLabel 2350 6550 0    50   Input ~ 0
W.~SD
Text HLabel 2350 6450 0    50   Input ~ 0
W.IN
$Comp
L Driver_FET:IR2184 U?
U 1 1 5F1B89D4
P 3600 6450
AR Path="/5F1B89D4" Ref="U?"  Part="1" 
AR Path="/5F184EC4/5F1B89D4" Ref="U7"  Part="1" 
AR Path="/61B07F66/5F1B89D4" Ref="U?"  Part="1" 
AR Path="/61B81A35/5F1B89D4" Ref="U7"  Part="1" 
F 0 "U7" H 3700 6900 50  0000 C CNN
F 1 "IR2184" H 3800 6000 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3600 6450 50  0001 C CIN
F 3 "" H 3600 6450 50  0001 C CNN
F 4 "C2995" H 3600 6450 50  0001 C CNN "LCSC"
	1    3600 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5F385799
P 4100 6450
AR Path="/5F184EC4/5F385799" Ref="C11"  Part="1" 
AR Path="/61B07F66/5F385799" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F385799" Ref="C11"  Part="1" 
F 0 "C11" H 4215 6496 50  0000 L CNN
F 1 "10u" H 4215 6405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4138 6300 50  0001 C CNN
F 3 "~" H 4100 6450 50  0001 C CNN
F 4 "C13585" H 4100 6450 50  0001 C CNN "LCSC"
	1    4100 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5F3E2B0A
P 3100 5900
AR Path="/5F184EC4/5F3E2B0A" Ref="C8"  Part="1" 
AR Path="/61B07F66/5F3E2B0A" Ref="C?"  Part="1" 
AR Path="/61B81A35/5F3E2B0A" Ref="C8"  Part="1" 
F 0 "C8" H 3215 5946 50  0000 L CNN
F 1 "10u" H 3215 5855 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3138 5750 50  0001 C CNN
F 3 "~" H 3100 5900 50  0001 C CNN
F 4 "C13585" H 3100 5900 50  0001 C CNN "LCSC"
	1    3100 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 5F411371
P 3850 5650
AR Path="/5F184EC4/5F411371" Ref="D3"  Part="1" 
AR Path="/61B07F66/5F411371" Ref="D?"  Part="1" 
AR Path="/61B81A35/5F411371" Ref="D3"  Part="1" 
F 0 "D3" H 3850 5433 50  0000 C CNN
F 1 "US1M" H 3850 5524 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 3850 5650 50  0001 C CNN
F 3 "~" H 3850 5650 50  0001 C CNN
F 4 "C412437" H 3850 5650 50  0001 C CNN "LCSC"
	1    3850 5650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R17
U 1 1 5F1686BF
P 4800 6250
AR Path="/5F184EC4/5F1686BF" Ref="R17"  Part="1" 
AR Path="/61B07F66/5F1686BF" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F1686BF" Ref="R17"  Part="1" 
F 0 "R17" H 4870 6296 50  0000 L CNN
F 1 "4.7" H 4870 6205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 6250 50  0001 C CNN
F 3 "~" H 4800 6250 50  0001 C CNN
F 4 "C17675" H 4800 6250 50  0001 C CNN "LCSC"
	1    4800 6250
	0    -1   1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 5F1739E3
P 4800 6750
AR Path="/5F184EC4/5F1739E3" Ref="R18"  Part="1" 
AR Path="/61B07F66/5F1739E3" Ref="R?"  Part="1" 
AR Path="/61B81A35/5F1739E3" Ref="R18"  Part="1" 
F 0 "R18" H 4870 6796 50  0000 L CNN
F 1 "4.7" H 4870 6705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 6750 50  0001 C CNN
F 3 "~" H 4800 6750 50  0001 C CNN
F 4 "C17675" H 4800 6750 50  0001 C CNN "LCSC"
	1    4800 6750
	0    -1   1    0   
$EndComp
Wire Wire Line
	4100 6650 5100 6650
Wire Wire Line
	4950 6750 5100 6750
Wire Wire Line
	4950 6250 5100 6250
Wire Wire Line
	5000 6850 5100 6850
Wire Wire Line
	5000 6950 5000 6850
$Comp
L power:GND #PWR?
U 1 1 5F893C05
P 5000 6950
AR Path="/5F893C05" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F893C05" Ref="#PWR0113"  Part="1" 
AR Path="/61B07F66/5F893C05" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F893C05" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 5000 6700 50  0001 C CNN
F 1 "GND" H 5005 6777 50  0000 C CNN
F 2 "" H 5000 6950 50  0001 C CNN
F 3 "" H 5000 6950 50  0001 C CNN
	1    5000 6950
	1    0    0    -1  
$EndComp
Text HLabel 5100 6850 2    50   Output ~ 0
W.LS
Text HLabel 5100 6750 2    50   Output ~ 0
W.LO
Text HLabel 5100 6650 2    50   Output ~ 0
W.HS
Text HLabel 5100 6250 2    50   Output ~ 0
W.HO
Wire Wire Line
	3900 6750 4650 6750
Connection ~ 4100 6650
Wire Wire Line
	3900 6250 4650 6250
Connection ~ 4100 6150
Wire Wire Line
	4100 5650 4100 6150
Wire Wire Line
	4000 5650 4100 5650
Wire Wire Line
	3600 5650 3700 5650
Wire Wire Line
	3600 5650 3600 5950
Connection ~ 3600 5650
Wire Wire Line
	3600 5550 3600 5650
Wire Wire Line
	3100 5650 3100 5750
Wire Wire Line
	3600 5650 3100 5650
$Comp
L power:GND #PWR?
U 1 1 5F3E2B14
P 3100 6150
AR Path="/5F3E2B14" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F3E2B14" Ref="#PWR0105"  Part="1" 
AR Path="/61B07F66/5F3E2B14" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F3E2B14" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 3100 5900 50  0001 C CNN
F 1 "GND" H 3105 5977 50  0000 C CNN
F 2 "" H 3100 6150 50  0001 C CNN
F 3 "" H 3100 6150 50  0001 C CNN
	1    3100 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6650 4100 6600
Wire Wire Line
	3900 6650 4100 6650
Wire Wire Line
	4100 6150 4100 6300
Wire Wire Line
	3900 6150 4100 6150
Wire Wire Line
	3300 6450 2350 6450
Wire Wire Line
	2350 6550 3300 6550
$Comp
L power:VDC #PWR?
U 1 1 5F1B89B1
P 3600 5550
AR Path="/5F1B89B1" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B89B1" Ref="#PWR08"  Part="1" 
AR Path="/61B07F66/5F1B89B1" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B89B1" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 3600 5450 50  0001 C CNN
F 1 "VDC" H 3615 5723 50  0000 C CNN
F 2 "" H 3600 5550 50  0001 C CNN
F 3 "" H 3600 5550 50  0001 C CNN
	1    3600 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F1B89AB
P 3600 6950
AR Path="/5F1B89AB" Ref="#PWR?"  Part="1" 
AR Path="/5F184EC4/5F1B89AB" Ref="#PWR09"  Part="1" 
AR Path="/61B07F66/5F1B89AB" Ref="#PWR?"  Part="1" 
AR Path="/61B81A35/5F1B89AB" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 3600 6700 50  0001 C CNN
F 1 "GND" H 3605 6777 50  0000 C CNN
F 2 "" H 3600 6950 50  0001 C CNN
F 3 "" H 3600 6950 50  0001 C CNN
	1    3600 6950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
