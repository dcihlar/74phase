# 74phase

![PCB render](/img/74phase.png)

3 phase IGBT driver implemented with 74 series logic gates and
some bits of analog electronics for timing.

## Features

 * Selectable single side and two side delta driving mode
 * Adjustable output power (16kHz PWM)
 * 50 Hz output frequency
 * External oscillator
 * Up to 600V output
 * Reduced BOM with external control

## Pinout

Note that the pinout is also visible on the underside of the PCB.

### J1

J1 is the connector for configuration and power supply.

| Pin | Name       | Function       |
|-----|------------|----------------|
| 1   | `VDC`      | Supply in      |
| 2   | `GND`      | Ground         |
| 3   | `VCC`      | 5V out         |
| 4   | `EXT\_CLK` | External clock |
| 5   | `EN`       | Enable in      |
| 6   | `MODE`     | Mode select    |
| 7   | `PWR\_SEL` | Power select   |
| 8   | `GND`      | Ground         |

### J2

J2 is the connector for IGBTs.
See IR2184 datasheet for more details.

It is recommended that every pair of pins is wired with a
twisted pair wire.

| Pin | Name   |
|-----|--------|
| 1   | `W.HO` |
| 2   | `W.HS` |
| 3   | `W.LO` |
| 4   | `W.LS` |
| 5   | `V.HO` |
| 6   | `V.HS` |
| 7   | `V.LO` |
| 8   | `V.LS` |
| 9   | `U.HO` |
| 10  | `U.HS` |
| 11  | `U.LO` |
| 12  | `U.LS` |

## Configuration

### Enabling

`EN` pin enables or disables output.
It must be set between 2.5V and 5V to become active.

`EN` can be used to protect IGBTs and the whole system by connecting
thermal switches or similar devices in series.

It can be also used to protect batteries from over discharging.

### Power output

Power output is selected by voltage on `PWR_SEL`.
0V corresponds to no output, and 5V to maximum power.
A potentiometer can be used for manual adjustment.

### Delta mode

Two output modes are available:
 * driving two, or
 * driving only one side of a three phase triangle.

Setting `MODE` to 5V selects one side mode,
and setting it to 0V or leaving it open selects two sides mode.

### Custom frequency

Internal output frequency is set to 50Hz.
It can be changed by changing `R1` or `C1`.

`JP1` can be disconnected to allow external frequency control.

### Full external control

If full external control of PWM and frequency is desired
(i.e. with an Arduino with LCD, and all bells and whistles)
then many of the components don't even have to be soldered.

`U10`, `U11`, `Q1` and the belonging passive components aren't required.
`U10` must not be installed because on its place are two solder jumpers
(`JP3`, and `JP1`) that need to be bridged.
